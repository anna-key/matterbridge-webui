window.onerror= function(msg,url,line,col,evt){
  evt.preventDefault();evt.stopPropagation();
  alert(
    (msg.toLowerCase().indexOf("script error") > -1)
    ? 'Script Error: See Browser Console for Detail'
    : msg+"\n"+url+"\nLine: "+line+' Col: '+col
    );
  return false;
  };
function el(id){
  return document.getElementById(id);
  };

var section;
var editor= CodeMirror.fromTextArea(
  document.forms[0].content,
  { mode: {name:"toml"}, lineNumbers: true }
  );

el('matter').addEventListener('click',evt=>{
  evt.preventDefault();evt.stopPropagation();
  let btn= evt.target.closest('input');
  if(!btn) return;
  editor.setValue(editor.getValue()+"\n"+section[btn.value]);
  });

fetch('matterbridge.toml').then(res=>res.text()).then(data=>{
  editor.setValue(data);
  }).catch(e=>{alert(str + e.toString())});

fetch('matterbridge.toml.sections').then(res=>res.json())
  .then(data=>{
  section= data;
  let tgt= el('matter');
  for(let key of Object.keys(data)){
    btn= document.createElement('input');
    btn.type= 'button';
    btn.value= key;
    tgt.appendChild(btn);
    }
  }).catch(e=>{alert(str + e.toString())});

el('reset').addEventListener('click',function(evt){
  evt.preventDefault();evt.stopPropagation();
  editor.setValue("");
  });

el('submit').addEventListener('click',function(evt){
  evt.preventDefault();evt.stopPropagation();
  let toml= editor.getValue();
  //! freezes on invalid data. yeah
  //the only working parser out of 4
  //write your own parser or spawn a worker with timeout
  //or abuse traffic(serverside.parse)
  //...but hey who cares since
  //google started flooding the world with suggestions
  //crap crap crap crap crepes
  if(toml.trim().length &&
    !TOML.parse(toml)){ alert('invalid data'); return; }
  var formData = new FormData();
  formData.append("toml", toml);
  var r= new XMLHttpRequest();
  r.addEventListener("load",evt=>alert(r.statusText));
  r.addEventListener("error",evt=>alert('bad connection'));
  r.open("POST", "save.php");
  r.send(formData);
  });
