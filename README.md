## matterbridge-webui
### License: Public Domain
#### Note:

 - index.js handles POST solely with HTTP response code
   203 for success and 400 for bad request, no content body.
 - it is assumed exactly one toml configfile to be edited
 - the layout is responsive
 - the repo as is, is not suitable for production
 - serverside php script is 
   1. to convert toml to json toml.sections
   2. simple save method
   3. https and auth is assumed per server config

#### Issues:

 - the convert folder should be placed outside server-root.
 - the matterbridge.toml.sample has to manually corrected before converted to json toml.sections
 - toml-filespec in general is not best choice, encoding json to toml is not trivial and having comments embedded as well, makes cost-benefit relation ridicilous.
 - last and least : from 1 out of 4 toml-js (parser only(of course)) libraries worked and this one freezes the script on invalid toml data. a real bummer

